/*
  Simulation of the PS-ESS

  1/9/15  001 First try. No B-field from ECR solenoids. Geometry from drawing. Scan mesh size. 
  9/9/15  002 Scan number of trajectories. Mesh = 0.4 mm. 
  9/6/16  003 Output emittance at the interface between the pumping box and cross piece (475 mm). For the doppler shift measurement. 
  16/6/16 001 Simulation with solenoid1
  17/6/16 002 Only protons 
  24/6/16 003 With B-field from ion source
  29/6/16 004 Full LEBT
  20/7/16 005 Simulation with only p+ and H2+ for tracewin
  8/8/16  007 Kill SCC after repeller electrode
  9/8/16  008 Include repeller electrode. 80% SCC in extraction column. 
  16/2/17 010 Updated LEBT lattice
  24/2/17 011 Solenoid 1 closest possible to ion source
  2/3/17  012 Simulate only ion source to compare with emittance measurements. -1.5 kV on electron repeller. 
  17/3/17 014 For Benjamin: tracewinfile between solenoids. 1e5 particles at 1600 mm. 
  12/4/17 015 Check with tracewin geometry. For tracewin (output at 68 mm): 80% H+, 20% H2+, SCC 95% from 40 mm to repeller, I=95mA, sol1 0.25, sol2 0.16. 
  20/4/17 017 New plasma electrode geometry. Plasma surface at 2 mm. 
  12/6/17 019 Include the commissioning tank. Chopper aperture 80 mm. 
  7/3/18  020 Move puller 1 mm closer to plasma chamber. Try to match Lorenzo measurements. 84 mA extracted, 70 mA to RFQ. Guess: 85% H+, 12% H2+, 3% H3+. SCC unknown.  
*/
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <iostream>
#include <ibsimu.hpp>
#include <error.hpp>
#include <geometry.hpp>
#include <geomplotter.hpp>
#include <func_solid.hpp>
#include <dxf_solid.hpp>
#include <mydxffile.hpp>
#include <epot_field.hpp>
#include <epot_efield.hpp>
#include <meshvectorfield.hpp>
#include <epot_bicgstabsolver.hpp>
#include <trajectorydiagnostics.hpp>
#include <particledatabase.hpp>
#include <particlediagplotter.hpp>

#ifdef GTK3
#include "gtkplotter.hpp"
#endif

using namespace std;

string commonFolder = "/storage2/oysteinmidttun/simulations/";
string outputFolder = "./";

string geometryFile = commonFolder+"geometry/ps-ess_v3.dxf" ;
string bFieldSol1 = commonFolder+"magField/bFieldSol1_2017.txt" ;
string bFieldSol2 = commonFolder+"magField/bFieldSol2_2017.txt" ;
//string bFieldSource = commonFolder+"magField/bFieldSource.txt" ;

/*
void add_beam( Geometry &geom, ParticleDataBaseCyl &pdb, double q, double m, 
	       double Jtotal, double frac )
{
    pdb.add_2d_beam_with_energy( Npart*frac, Jtotal*frac, q, m,
				 E0, 0.0, Tt,
				 geom.origo(0), 0,
				 geom.origo(0), r0 );
}
*/


bool extraction( double x, double y, double z )
{
  return( x > 66.0e-3 && x <= 253.1e-3 && y >= 50.0e-3 );
}
bool pumpingBox( double x, double y, double z )
{
  return( x > 253.1e-3 && x <= 453.1e-3 && y >= 75.0e-3 );
}
bool solenoid1( double x, double y, double z )
{
  return( x > 453.1e-3 && x <= 616.2e-3 && y >= 50.0e-3 );
}
bool sol2iris( double x, double y, double z )
{
  return( x > 616.2e-3 && x <= 1076.9e-3 && y >= 75.0e-3 );
}
bool iris( double x, double y, double z )
{
  return( x > 1076.9e-3 && x <= 1091.9e-3 && y >= 40.0e-3 );
}
bool iris2chopper( double x, double y, double z )
{
  return( x > 1091.9e-3 && x <= 1426.9e-3 && y >= 75.0e-3 );
}
bool chopper( double x, double y, double z )
{
  return( x > 1426.9e-3 && x <= 1521.9e-3 && y >= 40.0e-3 );
}
bool diagBox( double x, double y, double z )
{
  return( x > 1521.9e-3 && x <= 1846.4e-3 && y >= 75.0e-3 );
}
bool solenoid2( double x, double y, double z )
{
  return( x > 1846.4e-3 && x <= 2240.0e-3 && y >= 50.0e-3 );
}
bool sol2col( double x, double y, double z )
{
  return( x > 2240.0e-3 && x <= 2379.9e-3 && y >= 75.0e-3 );
}
bool collimator( double x, double y, double z )
{
  if( y <= 35.0e-3 - (x-2379.9e-3) * 28/173 )
    return( false );
  return( x > 2379.9e-3 && x <= 2554.9e-3 && y >= 7.0e-3 );
}
/*
bool collimator2( double x, double y, double z )
{
  return( x > 2552.9e-3 && x <= 2554.9e-3 && y >= 7.0e-3 );
}
*/
bool repeller( double x, double y, double z )
{
  return( x > 2555.9e-3 && x <= 2558.9e-3 && y >= 8.0e-3 );
}
bool rfq_int( double x, double y, double z )
{
  return( x > 2559.9e-3 && x <= 2575.9e-3 && y >= 7.5e-3 );
}
bool com_tank( double x, double y, double z )
{
  return( x > 2575.9e-3 && x <= 3405.9e-3 && y >= 79.0e-3 );
}
/*
bool beamPipe( double x, double y, double z )
{
  if( x >= 66.0e-3 && x <= 253.0e-3 && y <= 50.0e-3 )
    return( false );
  if( x >= 253.0e-3 && x <= 475.0e-3 && y <= 60.0e-3 )
    return( false );
  if( x >= 475.0e-3 && x <= 640.0e-3 && y <= 50.0e-3 )
    return( false );
  if( x >= 640.0e-3 && x <= 1100.0e-3 && y <= 60.0e-3 )
    return( false );
  if( x >= 1120.0e-3 && x <= 1450.0e-3 && y <= 60.0e-3 )
    return( false );
  if( x >= 1550.0e-3 && x <= 1921.0e-3 && y <= 60.0e-3 )
    return( false );
  if( x >= 1921.0e-3 && x <= 2021.0e-3 && y <= 50.0e-3 )
    return( false );
  if( x >= 2021.0e-3 && x <= 2396.0e-3 && y <= 60.0e-3 )
    return( false );
  if( x >= 2396.0e-3 && x <= 2700.0e-3 && y <= 50.0e-3 )
    return( false );
  
  return( x >= 66.0e-3 && y >= 40.0e-3 );

}
*/
/*
bool faradayCup( double x, double y, double z )
{
  return( x >= 2580.0e-3 );
}
*/
bool faradayCup( double x, double y, double z )
{
  return( x >= 3235.9e-3 && y <= 40.0e-3 );
}

void simu( int argc, char **argv )
{
  if (argc!=6) {
    std::cout<<"Not the right number of parameters\n";
    exit(1);
  }

  //double q = 6;
  //double m = 15;
  //double B0 = 0.9;
  //double r_aperture = 4.0e-3;
  //double vz = sqrt(-2.0*q*CHARGE_E*Vgnd/(m*MASS_U));
  //double Erms = q*CHARGE_E*B0*r_aperture*r_aperture/(8*m*MASS_U*vz);
  //ibsimu.message(1) << "Erms = "<< Erms << " m rad\n";
  
  double Vplasma = 0;
  //double Vpuller = atof(argc[5]) * -1e3;
  //double Vrepeller = -78.5e3;
  //double Vground = -75e3;
  double Vpuller = atof(argv[5]) * -1e3;
  double Vrepeller = atof(argv[5]) * -1e3 - 3.5e3;
  double Vground = atof(argv[5]) * -1e3;

  int Nrounds = 15;
  double h = 0.4e-3;//atof(argv[1]) * 1e-3;
  double aperture = 4.0e-3;//atof(argv[3])*1.0e-3;
  double nperh = 12000;//atof(argv[1]);//500;
  double r0 = 8e-3;
  double Npart = r0/h*nperh;
  double Jtotal = atof(argv[1])*1.0e-3/(aperture*aperture*M_PI);//74.0e-3;
  double Hfrac = 0.85;
  double H2frac = 0.12;
  double H3frac = 0.03;
  double Te = 10.0;
  double E0 = 10;
  double Tt = 1.0;
  double Up = 20;
  double sc_alpha = 0.7;
  double scc = atof(argv[2]) * 1e-2;
  double sol1 = atof(argv[3]) / 0.20066;
  double sol2 = atof(argv[4]) / 0.20066;   
  
  Vec3D origin( -2e-3, 0, 0 );
  Vec3D sizereq( 72.2e-3, 80e-3, 0 );
  //Vec3D sizereq( 2580e-3, 80e-3, 0 );
  Int3D size( floor(sizereq[0]/h)+1,
	      floor(sizereq[1]/h)+1,
	      1 );
  Geometry geom( MODE_CYL, size, origin, h );
  
  MyDXFFile *dxffile = new MyDXFFile;
  dxffile->set_warning_level( 1 );
  dxffile->read( geometryFile );
  
  DXFSolid *s1 = new DXFSolid( dxffile, "1" );
  s1->scale( 1e-3 );
  geom.set_solid( 7, s1 );
  DXFSolid *s2 = new DXFSolid( dxffile, "2" );
  s2->scale( 1e-3 );
  s2->translate( Vec3D(-1e-3,0,0) );
  geom.set_solid( 8, s2 );
  DXFSolid *s3 = new DXFSolid( dxffile, "3" );
  s3->scale( 1e-3 );
  geom.set_solid( 9, s3 );
  DXFSolid *s4 = new DXFSolid( dxffile, "4" );
  s4->scale( 1e-3 );
  geom.set_solid( 10, s4 );
  Solid *s5 = new FuncSolid( extraction );
  geom.set_solid( 11, s5 );
  Solid *s6 = new FuncSolid( pumpingBox );
  geom.set_solid( 12, s6 );
  Solid *s7 = new FuncSolid( solenoid1 );
  geom.set_solid( 13, s7 );
  Solid *s8 = new FuncSolid( sol2iris );
  geom.set_solid( 14, s8 );
  Solid *s9 = new FuncSolid( iris );
  geom.set_solid( 15, s9 );
  Solid *s10 = new FuncSolid( iris2chopper );
  geom.set_solid( 16, s10 );
  Solid *s11 = new FuncSolid( chopper );
  geom.set_solid( 17, s11 );
  Solid *s12 = new FuncSolid( diagBox );
  geom.set_solid( 18, s12 );
  Solid *s13 = new FuncSolid( solenoid2 );
  geom.set_solid( 19, s13 );
  Solid *s14 = new FuncSolid( sol2col );
  geom.set_solid( 20, s14 );
  Solid *s15 = new FuncSolid( collimator );
  geom.set_solid( 21, s15 );
  //Solid *s16 = new FuncSolid( collimator2 );
  //geom.set_solid( 22, s16 );
  Solid *s16 = new FuncSolid( repeller );
  geom.set_solid( 22, s16 );
  Solid *s17 = new FuncSolid( rfq_int );
  geom.set_solid( 23, s17 );
  Solid *s18 = new FuncSolid( faradayCup );
  geom.set_solid( 24, s18 );  
  Solid *s19 = new FuncSolid( com_tank );
  geom.set_solid( 25, s19 );  
  
  geom.set_boundary( 1, Bound(BOUND_NEUMANN, 0.0) ); // xmin
  geom.set_boundary( 2, Bound(BOUND_NEUMANN, 0.0) ); // xmax
  geom.set_boundary( 3, Bound(BOUND_NEUMANN, 0.0) ); // rmin
  geom.set_boundary( 4, Bound(BOUND_NEUMANN, 0.0) ); // rmax
  
  geom.set_boundary(  7, Bound(BOUND_DIRICHLET, Vplasma) );
  geom.set_boundary(  8, Bound(BOUND_DIRICHLET, Vpuller) );
  geom.set_boundary(  9, Bound(BOUND_DIRICHLET, Vrepeller) );
  geom.set_boundary( 10, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 11, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 12, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 13, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 14, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 15, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 16, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 17, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 18, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 19, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 20, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 21, Bound(BOUND_DIRICHLET, Vground) );
  //geom.set_boundary( 22, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 22, Bound(BOUND_DIRICHLET, Vrepeller) );
  geom.set_boundary( 23, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 24, Bound(BOUND_DIRICHLET, Vground) );
  geom.set_boundary( 25, Bound(BOUND_DIRICHLET, Vground) );

  geom.build_mesh();
  
  EpotField epot( geom );
  MeshScalarField scharge( geom );
  MeshScalarField scharge_ave( geom );
  bool fsel[3] = {true, true, false};
  //  bool fout[3] = {true, true, true};
  //MeshVectorField bfield;
  MeshVectorField bfield( MODE_CYL, fsel, 1.0e-3, sol1, bFieldSol1 );
  MeshVectorField bfield2( MODE_CYL, fsel, 1.0e-3, sol2, bFieldSol2 );
  //MeshVectorField bfield2( MODE_CYL, fsel, 1.0e-3, 1.0, bFieldFile );
  //MeshVectorField bsource( MODE_CYL, fsel, 1.0e-3, 1.0, bFieldSource );
  
  field_extrpl_e bfldextrpl[6] = { FIELD_ZERO, FIELD_ZERO,
                                   FIELD_ZERO, FIELD_ZERO,
                                   FIELD_ZERO, FIELD_ZERO };
  bfield.set_extrapolation( bfldextrpl );
  
    bfield2.set_extrapolation( bfldextrpl );
  //bsource.set_extrapolation( bfldextrpl );  

  //bfield.translate( Vec3D(300e-3,0,0) );
  //  bfield2.translate( Vec3D(1746e-3,0,0) );
  //bsource.translate( Vec3D(-100e-3,0,0) );
    /*  
  MeshVectorField bfield ( MODE_3D,
			   fout,
			   size,
			   origin,
			   h,
			   bsol );
  MeshVectorField bfield2( MODE_3D,
			   fout,
			   size,
			   origin,
			   h,
			   bsource );
    */
  bfield += bfield2;
  

  EpotEfield efield( epot );
  field_extrpl_e extrapl[6] = { FIELD_EXTRAPOLATE, FIELD_EXTRAPOLATE,
				FIELD_SYMMETRIC_POTENTIAL, FIELD_EXTRAPOLATE,
				FIELD_EXTRAPOLATE, FIELD_EXTRAPOLATE };
  efield.set_extrapolation( extrapl );
  
  EpotBiCGSTABSolver solver( geom );
  InitialPlasma init_plasma( AXIS_X, 2.0e-3 );
  solver.set_initial_plasma( Up, &init_plasma );
  
  ParticleDataBaseCyl pdb( geom );
  bool pmirror[6] = {false, false,
		     true, false,
		     false, false};
  pdb.set_mirror( pmirror );
  
  for( int a = 0; a < Nrounds; a++ ) {
    
    ibsimu.message(1) << "Major cycle " << a << "\n";
    ibsimu.message(1) << "-----------------------\n";
    
    if( a == 1 ) {
      double rhoe = pdb.get_rhosum();
      solver.set_pexp_plasma( rhoe, Te, Up );
    }
    
    solver.solve( epot, scharge_ave );
    if( solver.get_iter() == 0 ) {
      ibsimu.message(1) << "No iterations, breaking major cycle\n";
      break;
    }
    
    efield.recalculate();
    
    pdb.clear();
    pdb.add_2d_beam_with_energy( Npart*H3frac, Jtotal*H3frac, 1, 3.021828,
				 E0, 0.0, Tt,
				 geom.origo(0), 0,
				 geom.origo(0), r0 );
    
    pdb.add_2d_beam_with_energy( Npart*H2frac, Jtotal*H2frac, 1, 2.014552,
				 E0, 0.0, Tt,
				 geom.origo(0), 0,
				 geom.origo(0), r0 );
    pdb.add_2d_beam_with_energy( Npart*Hfrac, Jtotal*Hfrac, 1, 1.007276,
				 E0, 0.0, Tt,
				 geom.origo(0), 0,
				 geom.origo(0), r0 );
    
    pdb.iterate_trajectories( scharge, efield, bfield );    

    ParticleStatistics stat =pdb.get_statistics();
    double end=stat.bound_current(2);
    double puller=stat.bound_current(8);
    double ground=stat.bound_current(10);
    double iris=stat.bound_current(15);
    double chopper=stat.bound_current(17);
    double collimator=stat.bound_current(21);
    //double fcup=stat.bound_current(24);

    double srcCurrent = 0;
    double srcCurrent_p = 0;
    double endCurrent = 0;
    double endCurrent_p = 0;
    for( size_t i = 0; i < pdb.size(); i++ ) {
      ParticleCyl &pp = pdb.particle(i);
      if( pp(1) > 70.0e-3 ) srcCurrent += pp.IQ();
      if( pp(1) > 70.0e-3 && pp.m() < 1.5*MASS_U ) srcCurrent_p += pp.IQ();
      if( pp(1) > 2576.0e-3 ) endCurrent += pp.IQ();
      if( pp(1) > 2576.0e-3 && pp.m() < 1.5*MASS_U ) endCurrent_p += pp.IQ();
    }

    TrajectoryDiagnosticData tdata;
    vector<trajectory_diagnostic_e> diag;
    diag.push_back( DIAG_R );
    diag.push_back( DIAG_RP );
    diag.push_back( DIAG_AP );
    diag.push_back( DIAG_CURR );

    pdb.trajectories_at_plane( tdata, AXIS_X, 70.0e-3, diag );
    EmittanceConv emit( 100, 100, 
			tdata(0).data(), tdata(1).data(), 
			tdata(2).data(), tdata(3).data() );
    pdb.trajectories_at_plane( tdata, AXIS_X, 1600.0e-3, diag );
    EmittanceConv emit2( 100, 100, 
			tdata(0).data(), tdata(1).data(), 
			tdata(2).data(), tdata(3).data() );
    pdb.trajectories_at_plane( tdata, AXIS_X, 2575.9e-3, diag );
    EmittanceConv emit3( 100, 100, 
			tdata(0).data(), tdata(1).data(), 
			tdata(2).data(), tdata(3).data() );
    
    ofstream dataout( "./emit.txt", ios_base::app );
    dataout << srcCurrent << "\t"
	    << srcCurrent_p << "\t"
	    << endCurrent << "\t"
	    << endCurrent_p << "\t"
	    << end << "\t"
	    << puller << "\t"
	    << ground << "\t"
	    << iris << "\t"
      //<< fcup << "\t"
	    << chopper << "\t"
	    << collimator << "\t"
	    << emit.alpha() << "\t"
	    << emit.beta() << "\t"
	    << emit.epsilon() << "\t"
	    << emit2.alpha() << "\t"
	    << emit2.beta() << "\t"
	    << emit2.epsilon() << "\t"
	    << emit3.alpha() << "\t"
	    << emit3.beta() << "\t"
	    << emit3.epsilon() << "\n";
    dataout.close();
    
    //  Space charge compensation at x>35 mm
    for( uint32_t i = 0; i < geom.size(0); i++ ) {
      double x = geom.origo(0) + geom.h()*i;
      if( x > 35e-3 && x < 2557.4e-3) {
	for( uint32_t j = 0; j < geom.size(1); j++ )
	  scharge(i,j) *= 1-scc;
      }
    }
    
    if( a == 0 ) {
      scharge_ave = scharge;
    } else {
      double sc_beta = 1.0-sc_alpha;
      uint32_t nodecount = scharge.nodecount();
      for( uint32_t b = 0; b < nodecount; b++ ) {
	scharge_ave(b) = sc_alpha*scharge(b) + sc_beta*scharge_ave(b);
      }
    }
  }
  
  //geom.save( "geom.dat" );
  //epot.save( "epot.dat" );
  //pdb.save( "pdb.dat" );
  
  MeshScalarField tdens( geom );
  pdb.build_trajectory_density_field( tdens );
    
#ifdef GTK3
  if( false ) {    
    GTKPlotter plotter( &argc, &argv );
    plotter.set_geometry( &geom );
    plotter.set_epot( &epot );
    plotter.set_particledatabase( &pdb );
    plotter.set_efield( &efield );
    plotter.set_trajdens( &tdens );
    plotter.set_bfield( &bfield );
    plotter.set_scharge( &scharge );
    plotter.new_geometry_plot_window();
    plotter.run();
  }
#endif
  
  GeomPlotter gplotter( geom );
  gplotter.set_size( 10240, 7680 );
  gplotter.set_font_size( 16 );
  gplotter.set_epot( &epot );
  std::vector<double> eqlines;
  eqlines.push_back( -1.0 );
  eqlines.push_back( -2.0 );
  eqlines.push_back( -4.0 );
  eqlines.push_back( -8.0 );
  gplotter.set_eqlines_manual( eqlines );
  
  gplotter.set_particle_database( &pdb );
  gplotter.set_view( VIEW_XY );
  gplotter.plot_png( "./p.png" );

  gplotter.set_trajdens( & tdens );
  gplotter.set_fieldgraph_plot( FIELD_TRAJDENS );  // plot trajectory density
  gplotter.set_particle_div( 0 ); // plot zero particle traces
  gplotter.fieldgraph()->set_zscale( ZSCALE_RELLOG );
  gplotter.set_view( VIEW_XY );
  gplotter.plot_png( "./d.png" );

  ParticleDiagPlotter pplotter1( geom, pdb, AXIS_X, 0.0e-3, 
				 PARTICLE_DIAG_PLOT_HISTO2D, 
				 DIAG_Y, DIAG_YP );
  //pplotter1.set_ranges( -0.008, -0.15001, 0.004, 0.05 );
  pplotter1.set_font_size( 20 );
  pplotter1.set_size( 800, 600 );
  pplotter1.plot_png( "plasma2d_emit1.png" );

  ParticleDiagPlotter pplotter2( geom, pdb, AXIS_X, 0.0e-3, 
				 PARTICLE_DIAG_PLOT_SCATTER, 
				 DIAG_Y, DIAG_YP );
  pplotter2.set_font_size( 20 );
  pplotter2.set_size( 800, 600 );
  pplotter2.plot_png( "plasma2d_emit2.png" );

  
  ParticleDiagPlotter pplotter3( geom, pdb, AXIS_X, 70.0e-3, 
				 PARTICLE_DIAG_PLOT_HISTO2D, 
				 DIAG_Y, DIAG_YP );
  //pplotter3.set_ranges( -40e-3, -60e-3, -20e-3, -20e-3 ); 
  pplotter3.set_font_size( 20 );
  pplotter3.set_size( 800, 600 );
  pplotter3.plot_png( "plasma2d_emit3.png" );

  ParticleDiagPlotter pplotter4( geom, pdb, AXIS_X, 70.0e-3, 
				 PARTICLE_DIAG_PLOT_SCATTER, 
				 DIAG_Y, DIAG_YP );
  //pplotter4.set_ranges( -40e-3, -60e-3, -20e-3, -20e-3 ); 
  pplotter4.set_font_size( 20 );
  pplotter4.set_size( 800, 600 );
  pplotter4.plot_png( "plasma2d_emit4.png" );
    
  // Write path manager file
  // arguments: filename, energy,charge, and mass of reference particle, central position of plane, vector1, and vector2 defining plane
  string pathFile = outputFolder + "tracewin_path_format.txt";
  int ref_E = 75000;
  int ref_q = 1;
  double ref_m = 1.007276;
  double pos_z = 70e-3;
  double n_part = 1e7;
  pdb.export_path_manager_data(pathFile, ref_E, ref_q, ref_m, pos_z, n_part);
  
  // Write output file containing all particles
  ofstream fileOut( "particles_out.txt" );
  for( size_t k = 0; k < pdb.size(); k++ ) {
    
    ParticleCyl &pp = pdb.particle( k );
        
    // Skip ions not at the end
    if( pp(1) < 50.0e-3 )
      continue;
    
    // Plot particle I, m, coordinates
    // 3D has 7 coordinates
    fileOut << setw(12) << pp.IQ() << " ";
    fileOut << setw(12) << pp.m() << " ";
    for( size_t j = 0; j < 6; j ++ )
      fileOut << setw(12) << pp(j) << " ";
    fileOut << "\n";
  }
  fileOut.close();
  
  for( size_t a = 0; a < pdb.size(); a++ ) {
    ParticleCyl &pp = pdb.particle( a );
    if( pp.m() > 1.5*MASS_U ) {
      pdb.clear_trajectory(a);
    }
  }
  
  TrajectoryDiagnosticData p_tdata;
  vector<trajectory_diagnostic_e> p_diag;
  p_diag.push_back( DIAG_R );
  p_diag.push_back( DIAG_RP );
  p_diag.push_back( DIAG_AP );
  p_diag.push_back( DIAG_CURR );  
  for ( size_t x = 0; x < 26; x++ ) {
    pdb.trajectories_at_plane( p_tdata, AXIS_X, x*100.0e-3, p_diag );
    EmittanceConv p_emit( 100, 100, 
			p_tdata(0).data(), p_tdata(1).data(), 
			p_tdata(2).data(), p_tdata(3).data() );
    ofstream p_dataout( "./p_emit.txt", ios_base::app );
    p_dataout << p_emit.alpha() << "\t"
	    << p_emit.beta() << "\t"
	    << p_emit.epsilon() << "\n";
    p_dataout.close();    
  }


}

int main( int argc, char **argv )
{
  remove( "./emit.txt" );
  
  try {
    ibsimu.set_message_threshold( MSG_VERBOSE, 1 );
    ibsimu.set_thread_count( 2 );
    simu( argc, argv );
  } catch( Error e ) {
    e.print_error_message( ibsimu.message(0) );
  }
  
  return( 0 );
}
