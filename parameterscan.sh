#!/bin/bash

echo -e "universe = Vanilla\nshould_transfer_files = yes\n" > batch.job
echo -e "requirements = Machine == \"accws04.esss.lu.se\"\n" >> batch.job 
#echo -e "requirements = Machine == \"accws04.esss.lu.se\" || Machine == \"accws06.esss.lu.se\" || Machine == \"accws07.esss.lu.se\"\n" >> batch.job 
echo -e "getenv = True\n" >>batch.job
echo -e "stream_output = True\n" >>batch.job
echo -e "log            = condor.log \noutput         = condor.out \nerror          = condor.err\nexecutable     = simulation" >> batch.job
#echo -e "log            = condor.log \noutput         = condor.out \nerror          = condor.err\nexecutable     = simulation\nrequest_memory = 2007" >> batch.job

for c in 5 10 20 30 40 50 60 70 80 90 100
do
    for hv in 0.1 0.5 1 5 10 20 50 75
    do
	for scc in 95 
	#98 100
    do
 	for sol1 in 0
#0.12 0.125 0.13 0.135 0.14 0.145 0.15 0.155 0.16 0.165 0.17 0.175 0.18 0.185 0.19 0.195 0.2 0.205 0.21 0.215 0.22 0.225 0.23 0.235 0.24 0.245 0.25 0.255 0.26 0.265 0.27 0.275 0.28 0.285 0.29 0.295 0.3
	do
	    for sol2 in 0
#0.12 0.125 0.13 0.135 0.14 0.145 0.15 0.155 0.16 0.165 0.17 0.175 0.18 0.185 0.19 0.195 0.2 0.205 0.21 0.215 0.22 0.225 0.23 0.235 0.24 0.245 0.25 0.255 0.26 0.265 0.27 0.275 0.28 0.285 0.29 0.295 0.3

	    do
		mkdir c${c}scc${scc}sol1${sol1}sol2${sol2}hv${hv}
		echo -e "initialdir = c${c}scc${scc}sol1${sol1}sol2${sol2}hv${hv}" >> batch.job
		echo -e "arguments      = ${c} ${scc} ${sol1} ${sol2} ${hv}" >> batch.job
		echo -e "queue 1\n" >> batch.job
	    done
	done
    done
done
done
#condor_submit batch.job

