CC = g++
LDFLAGS = `pkg-config --libs ibsimu-1.0.6dev`
CXXFLAGS = -Wall -g `pkg-config --cflags ibsimu-1.0.6dev`

simulation: simulation.o
	$(CC) -o simulation simulation.o $(LDFLAGS)
simulation.o: simulation.cpp

clean:
	$(RM) *.o ecr 

